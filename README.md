# CS 373: Software Engineering - 11am Group 12 - **ConservOcean**

- Website: https://www.conservocean.me
- Git SHA: c7f708e9f547a770b879db001d3199f5deb4aaa7
- GitLab Pipelines: https://gitlab.com/joewallery/cs373-group12/-/pipelines

| Name (leader)             | EID     | GitLab ID      | Estimated Time (hours)                               | Actual Time (hours)                                  |
| ------------------------  | ------- | -------------- | ---------------------------------------------------- | ---------------------------------------------------- |
| Joseph Wallery (Phase 1)  | jbw2756 | @joewallery    | **I:** 15<br>**II:** 25<br>**III:** 25<br>**IV:** 22 | **I:** 28<br>**II:** 26<br>**III:** 29<br>**IV:** 24 |
| Andy Weng (Phase 3)       | aw33252 | @AndyWeng33252 | **I:** 20<br>**II:** 20<br>**III:** 20<br>**IV:** 10 | **I:** 22<br>**II:** 34<br>**III:** 18<br>**IV:** 11 |
| Christine Tsou            | cjt2538 | @ChristineTsou | **I:** 15<br>**II:** 20<br>**III:** 15<br>**IV:** 10 | **I:** 16<br>**II:** 34<br>**III:** 18<br>**IV:** 11 |
| Rishi Salem (Phase 2)     | ras5832 | @rishi312      | **I:** 15<br>**II:** 20<br>**III:** 25<br>**IV:** 22 | **I:** 20<br>**II:** 26<br>**III:** 30<br>**IV:** 24 |
| Serena Zamarripa(Phase 4) | snz252  | @renaz6        | **I:** 11<br>**II:** 10<br>**III:** 25<br>**IV:** 20 | **I:** 21<br>**II:** 26<br>**III:** 30<br>**IV:** 23 |
| Dane Strandboge           | des2923 | @DStrand1      | **I:** N/A<br>**II:** 20<br>**III:** 20<br>**IV:** 10| **I:** N/A<br>**II:** 34<br>**III:** 18<br>**IV:** 11|

- Comments:
